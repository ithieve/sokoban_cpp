/*
**	Sokoban In C++
**	Version D3U4
**
**
**	This is a simple sokoban game made in C++.
**	Features:
**		- Play In console
**		- Load maps from file
**		- Save score with name map and time.
**		- Has a timer and move calculater as score 
**		
** Currently can:
**	- Show title
**  - Read Maps from file
*/

//Library Includes
#include <iostream>
#include <fstream> //fileoutput
#include <sstream> //to convert string to num
#include <conio.h> //getch
#include <vector>  //vectors
#include <cstdlib> //basic c functions
#include <cmath>   //c maths functions
#include <iomanip> // manipulators
#include <cstdio>  //almost same as cstdlib
#include <ctime>   //time input required as seed for rng
#include <algorithm> //Sort function
#include <windows.h> //console functions
#include <string> //


#define STATUS gotoxy(0, mapLength+2);
using namespace std;

//Global variables

//Constants
const char playerChar = 36;
const char wallChar = 219;
const char spaceChar = ' ';
const char boxChar = 61;
const char locaChar = 89;






//string mapNumber; //The map number string 
int mapTotal=0;
string mapNumber="0"; 
string mapImage="";
int currentMapNumber;

/*****Fuction prototypes******/


void game(string mapNumber);

string mapImageBuilder(string mapNumber);

//Tools
void pressToContinue(void);
int checkMaps(int task);
void container(void);
int toInt(string str);
void getPlayerPos(int &x, int &y);


/****Function Testing area*****/

// Clears Buffer
void cl(void){
	cin.clear();
	fflush(stdin);	
	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
}

//set console cursor position to coordinate
void gotoxy( int x, int y )
    {
    COORD p = { x, y };
    SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), p );
    }

void hidecursor(bool h)
{
   HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
   CONSOLE_CURSOR_INFO info;
   info.dwSize = 100;
   info.bVisible = !h;
   SetConsoleCursorInfo(consoleHandle, &info);
}


void clearInfo(int x, int y){
	gotoxy(0,y);
	for(int i=0; i<x; ++i) {
		for(int j=0; j<70; ++j){
			cout<<" ";
		}	
		cout<<endl;
	}
	gotoxy(0,y);
}

void showTitle(){
	system("cls");
	cout<<"=============================="<<endl
		<<"+           SOKOBAN          +"<<endl
		<<"+          Version: D3U4       +"<<endl
		<<"+                            +"<<endl
		<<"=============================="<<endl
		<<"Controls: "<<endl
		<<"\ta,w,s,d to move"<<endl
		<<"\tq to quit"<<endl
		<<"\tt to back to title"<<endl
		<<"\tr to reset map"<<endl<<endl;
		
		cout<<"Maps Present: "<<mapTotal<<endl<<endl;

		cout<<"Enter Map number:";
}

void showGameStart(string mapNumber){
	cout<<"--------------------------------"<<endl
		<<"-                               "<<endl
		<<"-       Map: "<<mapNumber<<endl
		<<"-       Name:"<<endl
		<<"-                               "<<endl
		<<"--------------------------------"<<endl;
		pressToContinue();
}




//Prints the map on the screen
void printMap(string map){
	for(int i=0; i<map.length(); ++i){
			if(map[i]=='W') cout<<(char)219;
			else if(map[i]=='$') cout<<(char)36; 
			else if(map[i]=='B') cout<<(char)61;
			else if(map[i]=='#') cout<<(char)89;
			else if(map[i]=='.') cout<<" ";
			else if(map[i]=='\n') cout<<"\n";
			else if(map[i]=='V') cout<<" ";
	}
}





/*****MAIN PROGRAM******/

int main(void){
	container();
	
//	
//	
//	
//	
//	showTitle();
//	cin>>mapNumber;
//	system("cls");
//	
//	//Load Map
//	string map="";
//	string fileName="./maps/map";
//	fileName +=mapNumber;
//	fileName +=".txt";
//	
//	//Check file exists
//	if(ifstream(fileName.c_str())){
//	
//		//open File
//		ifstream inmap(fileName.c_str());
//	
//		//Read each lines
//		string line;
//		while(getline(inmap,line)){
//			map += line;
//			map +="\n";
//		}
//	}
//	else{
//		cout<<"The map: "<<fileName<<" does not exist";
//		exit(1);
//	}
//		
//	printMap(map);
//	cout<<endl<<endl;

//
//
//	for(int i=0; i<300; ++i){
//		cout<<i<<": "<<(char)i<<endl;
//	}	

}

//The main container function
void container(void){
	hidecursor(0);
	cl();
	mapTotal=checkMaps(0); //Check how many maps are present
	mapNumber="0"; 
	showTitle();
	cin>>mapNumber;
	if(checkMaps(1)){
		cout<<"Sorry, Map not available!"<<endl<<endl;
		pressToContinue();
		cl();
		container();
	}
	else {
		currentMapNumber=toInt(mapNumber);
		game(mapNumber);
	}
	
	
	
}

void game(string mapNumber){
	hidecursor(1);
	system("cls");
	//Build the Map Image
	string currentMapImage=mapImageBuilder(mapNumber);
	//initalize map layout array
	int mapLength=checkMaps(2);
	int mapWidth=checkMaps(3);
	toInt(mapNumber);
//	cout<<"Map: "<<currentMapNumber<<endl;
//	
//	cout<<"maplength: "<<mapLength<<endl;
//	cout<<"mapwidth: "<<mapWidth<<endl<<endl;
//	
	printMap(currentMapImage);
	
	char currentMapLayout[mapLength][mapWidth+1];
	int parser=0;
	for(int i=0; i<mapLength; ++i){
		for (int j=0; j<=mapWidth; ++j){
//			cout<<currentMapImage[parser];
			switch(currentMapImage[parser]){
				case 'W': currentMapLayout[i][j]='W'; break;
				case 'V': currentMapLayout[i][j]='V'; break;
				case '.': currentMapLayout[i][j]='.'; break;
				case 'B': currentMapLayout[i][j]='B'; break;
				case '$': currentMapLayout[i][j]='$'; break;
				case '#': currentMapLayout[i][j]='#'; break;
				case '\n': break;
				default: cout<<"error";
			}
			parser++;
		}
		cout<<endl;
	}
//	
//	cout<<endl<<endl;
//	for(int i=0; i<mapLength; ++i){
//		for(int j=0; j<mapWidth; ++j) 
//		{cout<<currentMapLayout[i][j];
//	
//			}	
//		cout<<endl;
//		
//	}
//	cout<<endl<<endl;
	
	//Get the Player Position 
	int playerPosX=0;
	int playerPosY=0;
	int foundIt=0;
	for (int i=0; !foundIt || i<= mapLength; ++i ){
		for(int j=0; !foundIt || j<=mapWidth; ++j){
			if(currentMapLayout[i][j]=='$'){
				playerPosX=j;
				playerPosY=i;
				foundIt=1;
			}
		}
	}
	
	if(foundIt==0){
		system("cls");
		cout<<"Error: Player not found on the map, Please try another map"<<endl;
		pressToContinue();
		cl();
		container();
	}
	
	
	//End 
	
	
	
	//The Game Player Loop
	int win=0;
	while(!win){
		char input;
		input=getch();
		
		if(input=='r') game(mapNumber);
		else if(input=='q') exit(1);
		else if(input=='t') container();
		
		//Moving Up
		else if(input=='w'){
			// If there is a wall
			if(currentMapLayout[playerPosY-1][playerPosX]=='W') {
				
				//print status
				clearInfo(5, mapLength+2);
				cout<<"Caracter Above: "<<currentMapLayout[playerPosY-1][playerPosX]<<endl;			
				cout<<"PlayerPosX: "<<playerPosX<<endl;
				cout<<"PlayerPosY: "<<playerPosY<<endl;
				cout<<"Info There is a wall in the top"<<endl	; //do Nothing
			}
			
			//If there is a box
			else if(currentMapLayout[playerPosY-1][playerPosX]=='B'){
				
				//If there is a wall in front of the box
				if(currentMapLayout[playerPosY-2][playerPosX]=='W'){
					clearInfo(5, mapLength+2);
					cout<<"Caracter Above: "<<currentMapLayout[playerPosY-1][playerPosX]<<endl;			
					cout<<"PlayerPosX: "<<playerPosX<<endl;
					cout<<"PlayerPosY: "<<playerPosY<<endl;
					cout<<"Info: The next character after the box is: "<<currentMapLayout[playerPosY-2][playerPosX]<<endl;
					cout<<"Info: There is a wall ahead of the box on top"<<endl	; //do Nothing
				}
				
				else{ //Move the player and the box UP
				
					currentMapLayout[playerPosY][playerPosX]='.'; //Set player position to an empty space
					gotoxy(playerPosX, playerPosY);
					cout<<" ";
					
					playerPosY-=1; //Move the Player Up
					currentMapLayout[playerPosY][playerPosX]='$';
					gotoxy(playerPosX, playerPosY);
					cout<<playerChar;
					
						//Move the Box
					currentMapLayout[playerPosY-1][playerPosX]='B';
					gotoxy(playerPosX, playerPosY-1);
					cout<<boxChar;
					
					//Print Status
					clearInfo(5, mapLength+2);
					cout<<"Caracter Above: "<<currentMapLayout[playerPosY-1][playerPosX]<<endl;			
					cout<<"PlayerPosX: "<<playerPosX<<endl;
					cout<<"PlayerPosY: "<<playerPosY<<endl;
					cout<<"Action : Player has been moved up from ("<<playerPosX<<","<<playerPosY+1<<") to ("<<playerPosX<<","<<playerPosY<<")."<<endl;
					cout<<"Action : Box has been moved up from ("<<playerPosX<<","<<playerPosY+2<<") to ("<<playerPosX<<","<<playerPosY+1<<")."<<endl;
					
				}
				
				
			}
			
			else if(currentMapLayout[playerPosY-1][playerPosX]=='.'){
				currentMapLayout[playerPosY][playerPosX]='.';
				gotoxy(playerPosX, playerPosY);
				cout<<" ";
				playerPosY-=1;
				currentMapLayout[playerPosY][playerPosX]='$';
				gotoxy(playerPosX, playerPosY);
				cout<<playerChar;
				
				//Print Stats
				clearInfo(5, mapLength+2);
				cout<<"Caracter Above: "<<currentMapLayout[playerPosY-1][playerPosX]<<endl;			
				cout<<"PlayerPosX: "<<playerPosX<<endl;
				cout<<"PlayerPosY: "<<playerPosY<<endl;
				cout<<"Action : Player has been moved up from ("<<playerPosX<<","<<playerPosY+1<<") to ("<<playerPosX<<","<<playerPosY<<")."<<endl;
			}
		}
		
		//Down
		else if(input=='s'){
			if(currentMapLayout[playerPosY+1][playerPosX]=='W') {
				clearInfo(5, mapLength+2);
				cout<<"Caracter below: "<<currentMapLayout[playerPosY+1][playerPosX]<<endl;		
				cout<<"PlayerPosX: "<<playerPosX<<endl;
				cout<<"PlayerPosY: "<<playerPosY<<endl;
				cout<<"there is a wall in the bottom"<<endl		; //do Nothing
			}
		}
		
		//Left
		else if(input=='a'){
			if(currentMapLayout[playerPosY][playerPosX-1]=='W') {
				
				//Print Status
				clearInfo(5, mapLength+2);
				cout<<"Caracter Left: "<<currentMapLayout[playerPosY][playerPosX-1]<<endl;
				cout<<"PlayerPosX: "<<playerPosX<<endl;
				cout<<"PlayerPosY: "<<playerPosY<<endl;
				cout<<"there is a wall in the left"<<endl		; //do Nothing
			}
		}
		
		//Right
		else if(input=='d'){
			
			if(currentMapLayout[playerPosY][playerPosX+1]=='W') {
				
				//Print Status
				clearInfo(5, mapLength+2);
				cout<<"Caracter Right: "<<currentMapLayout[playerPosY][playerPosX+1]<<endl;
				cout<<"PlayerPosX: "<<playerPosX<<endl;
				cout<<"PlayerPosY: "<<playerPosY<<endl;
				cout<<"there is a wall in the right"<<endl; //do Nothing
			}
		}
		
		
		
		
		
		
	}
	
	
	
	
}

string mapImageBuilder(string mapNumber){
	//Builds File name for the map
	string currentMapImage = "";
	string fileName = "./maps/map";
	fileName += mapNumber;
	fileName +=".txt";
	//Loads the file and builds the map Image
	ifstream inMap(fileName.c_str());
	string line;
	while(getline(inMap,line)){
		currentMapImage += line;
		currentMapImage +="\n";
		}
	//returns the mapImage
	return currentMapImage;
}


//Tool Function
int checkMaps(int task){
	if(task==0){
		//tells how many maps are there
		int num=0;
		int handle=1;
		do{
			char Buffer[10];
			sprintf(Buffer,"%d",num);
			string mapName="./maps/map";
			mapName += string(Buffer);
			mapName += ".txt";
			
			if(ifstream(mapName.c_str())){
				num++;
			}
			else{
				handle=0;
			}
		}
		while(handle);	
		return num;	
	}
	else if(task==1){
		//check if the map exists
		string mapName="./maps/map";
		mapName +=mapNumber;
		mapName +=".txt";
		if(!ifstream(mapName.c_str())) return 1;
		else return 0;	
	}	
	else if(task==2){
		//get map height
		int height=0;
		string mapName="./maps/map";
		mapName +=mapNumber;
		mapName +=".txt";
		string line;
		ifstream inMap(mapName.c_str());
		while(getline(inMap, line)){
			++height;
		}
		return height;
	}
	else if(task==3){
		//get map width
		int width=0;
		string mapName="./maps/map";
		mapName +=mapNumber;
		mapName +=".txt";
		string line;
		ifstream inMap(mapName.c_str());
		getline(inMap, line);
		while(line[width]){
			++width;
		}
		return width;
		
	}
}

int toInt(string str){
	int size=0; int multi=10; int number=0; int hold=0;
	while(str[size]) size++;
	for(int i=0; i<size; ++i){
		switch(str[i]){
			case '0': 
				number*=multi;
				number+=0;
				break;
			case '1': 
				number*=multi;
				number+=1;
				break;
			case '2': 
				number*=multi;
				number+=2;
				break;
			case '3': 
				number*=multi;
				number+=3;
				break;
			case '4': 
				number*=multi;
				number+=4;
				break;
			case '5': 
				number*=multi;
				number+=5;
				break;
			case '6': 
				number*=multi;
				number+=6;
				break;
			case '7': 
				number*=multi;
				number+=7;
				break;
			case '8': 
				number*=multi;
				number+=8;
				break;
			case '9': 
				number*=multi;
				number+=9;
				break;
		}
	}
	return number;
}


void pressToContinue(void){
    cout<<"Please ENTER to continue...";
    char c;
	cl();
	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
	while ((c = getchar()) != '\n' && c != EOF) { }
	cl();
}


