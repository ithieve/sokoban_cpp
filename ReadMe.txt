To make maps 
Simply name the maps as map1 or map2 or map3

then from the program just enter the map number to load the map

Rules to create map
Make Sure the characters are in CAPITAL LETTERS

V= void
W= wall
$=  player
.=  space
B= box
#= destination


the maps are defined to be a square world so fill up the space outside the wall
with V
The space INSIDE the wall should be filled with .

example map 
```
VVWWWWWWWWWWWWWWWWWWWV
VW..................WV
W..................WVV
W.$.................WV
W....B.......WWWW....W
VW...........WWWWWW##W
VVWWWWWWWWWWWWWVVVWWWW

this should become 

  ███████████████████
 █                  █
█                  █
█ $                 █
█    =       ████    █
 █           ██████YY█
  █████████████   ████
```