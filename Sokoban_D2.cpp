/*
**	Sokoban In C++
**	Version D2
**
**
**	This is a simple sokoban game made in C++.
**	Features:
**		- Play In console
**		- Load maps from file
**		- Save score with name map and time.
**		- Has a timer and move calculater as score 
**		
** Currently can:
**	- Show title
**  - Read Maps from file
*/

//Library Includes
#include <iostream>
#include <fstream> //fileoutput
#include <sstream> //to convert string to num
#include <conio.h> //getch
#include <vector>  //vectors
#include <cstdlib> //basic c functions
#include <cmath>   //c maths functions
#include <iomanip> // manipulators
#include <cstdio>  //almost same as cstdlib
#include <ctime>   //time input required as seed for rng
#include <algorithm> //Sort function
#include <windows.h> //console functions

using namespace std;

//Global variables
//string mapNumber; //The map number string 
int mapTotal=0;
string mapNumber="0"; 
string mapImage="";
	

/*****Fuction prototypes******/


void game(string mapNumber);

string mapImageBuilder(string mapNumber);

//Tools
void pressToContinue(void);
int checkMaps(int task);
void container(void);



/****Function Testing area*****/

void cl(void){
	cin.clear();
	fflush(stdin);	
	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
}



void showTitle(){
	system("cls");
	cout<<"=============================="<<endl
		<<"+           SOKOBAN          +"<<endl
		<<"+          Version: D2       +"<<endl
		<<"+                            +"<<endl
		<<"=============================="<<endl
		<<"Controls: "<<endl
		<<"\ta,w,s,d to move"<<endl
		<<"\tq to quit"<<endl
		<<"\tt to back to title"<<endl
		<<"\tr to reset map"<<endl<<endl;
		
		cout<<"Maps Present: "<<mapTotal<<endl<<endl;

		cout<<"Enter Map number:";
}

void showGameStart(string mapNumber){
	cout<<"--------------------------------"<<endl
		<<"-                               "<<endl
		<<"-       Map: "<<mapNumber<<endl
		<<"-       Name:"<<endl
		<<"-                               "<<endl
		<<"--------------------------------"<<endl;
		pressToContinue();
}




//Prints the map on the screen
void printMap(string map){
	for(int i; i<map.size(); ++i){

			if(map[i]=='W') cout<<(char)219;
			else if(map[i]=='$') cout<<(char)36; 
			else if(map[i]=='B') cout<<(char)61;
			else if(map[i]=='#') cout<<(char)89;
			else if(map[i]=='.') cout<<" ";
			else if(map[i]=='\n') cout<<"\n";
			else if(map[i]=='V') cout<<" ";
	}
}





/*****MAIN PROGRAM******/

int main(void){
	container();
	
//	
//	
//	
//	
//	showTitle();
//	cin>>mapNumber;
//	system("cls");
//	
//	//Load Map
//	string map="";
//	string fileName="./maps/map";
//	fileName +=mapNumber;
//	fileName +=".txt";
//	
//	//Check file exists
//	if(ifstream(fileName.c_str())){
//	
//		//open File
//		ifstream inmap(fileName.c_str());
//	
//		//Read each lines
//		string line;
//		while(getline(inmap,line)){
//			map += line;
//			map +="\n";
//		}
//	}
//	else{
//		cout<<"The map: "<<fileName<<" does not exist";
//		exit(1);
//	}
//		
//	printMap(map);
//	cout<<endl<<endl;

//
//
//	for(int i=0; i<300; ++i){
//		cout<<i<<": "<<(char)i<<endl;
//	}	

}

//The main container function
void container(void){
	
	cl();
	mapTotal=checkMaps(0); //Check how many maps are present
	mapNumber="0"; 
	showTitle();
	cin>>mapNumber;
	if(checkMaps(1)){
		cout<<"Sorry, Map not available!"<<endl<<endl;
		pressToContinue();
		cl();
		container();
	}
	else game(mapNumber);
	
	
	
}

void game(string mapNumber){
	
	//Build the Map Image
	string currentMapImage=mapImageBuilder(mapNumber);
	//initalize map layout array
	int mapLength=checkMaps(2);
	int mapWidth=checkMaps(3);
	
	cout<<"maplength: "<<mapLength<<endl;
	cout<<"mapwidth: "<<mapWidth<<endl<<endl;
	
	printMap(currentMapImage);
	
//	int currentMapLayout[mapLength][mapWidth];
	
}

string mapImageBuilder(string mapNumber){
	//Builds File name for the map
	string currentMapImage = "";
	string fileName = "./maps/map";
	fileName += mapNumber;
	fileName +=".txt";
	//Loads the file and builds the map Image
	ifstream inMap(fileName.c_str());
	string line;
	while(getline(inMap,line)){
		currentMapImage += line;
		currentMapImage +="\n";
		}
	//returns the mapImage
	return currentMapImage;
}


//Tool Function
int checkMaps(int task){
	if(task==0){
		//tells how many maps are there
		int num=0;
		int handle=1;
		do{
			char Buffer[10];
			sprintf(Buffer,"%d",num);
			string mapName="./maps/map";
			mapName += string(Buffer);
			mapName += ".txt";
			
			if(ifstream(mapName.c_str())){
				num++;
			}
			else{
				handle=0;
			}
		}
		while(handle);	
		return num;	
	}
	else if(task==1){
		//check if the map exists
		string mapName="./maps/map";
		mapName +=mapNumber;
		mapName +=".txt";
		if(!ifstream(mapName.c_str())) return 1;
		else return 0;	
	}	
	else if(task==2){
		//get map height
		int height=0;
		string mapName="./maps/map";
		mapName +=mapNumber;
		mapName +=".txt";
		string line;
		ifstream inMap(mapName.c_str());
		while(getline(inMap, line)){
			++height;
		}
		return height;
	}
	else if(task==3){
		//get map width
		int width=0;
		string mapName="./maps/map";
		mapName +=mapNumber;
		mapName +=".txt";
		string line;
		ifstream inMap(mapName.c_str());
		getline(inMap, line);
		while(line[width]!='\n'){
			++width;
		}
		return width;
		
	}
}

void pressToContinue(void){
    cout<<"Please ENTER to continue...";
    char c;
	cl();
	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
	while ((c = getchar()) != '\n' && c != EOF) { }
	cl();
}
