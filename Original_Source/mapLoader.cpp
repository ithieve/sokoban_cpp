#include <iostream>
#include <fstream>

using namespace std;

int main(){
    string fileName="map.txt";

    // Checking if file exists
    if(ifstream(fileName.c_str())){
        // open file for reading
        ifstream in(fileName.c_str());
        // reading a line a time from file
        string line;
        while(getline(in,line)){
            cout << line <<endl; // printing the line
        }
        // reading complete, closing file
        in.close();
    }else{
        cout << "Error: Map loading failed." << endl;
    }
}
