/*
**	Sokoban In C++
**	Version D1
**
**
**	This is a simple sokoban game made in C++.
**	Features:
**		- Play In console
**		- Load maps from file
**		- Save score with name map and time.
**		- Has a timer to 
**
*/

//Library Includes
#include <iostream>
#include <fstream> //fileoutput
#include <sstream> //to convert string to num
#include <conio.h> //getch
#include <vector>  //vectors
#include <cstdlib> //basic c functions
#include <cmath>   //c maths functions
#include <iomanip> // manipulators
#include <cstdio>  //almost same as cstdlib
#include <ctime>   //time input required as seed for rng
#include <algorithm> //Sort function
#include <windows.h> //console functions

using namespace std;

/*****Fuction prototypes******/

//Tools
void pressToContinue(void);




/****Function Testing area*****/

void cl(void){
	cin.clear();
	fflush(stdin);	
	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
}



void showTitle(){
	cout<<"=============================="<<endl
		<<"+           SOKOBAN          +"<<endl
		<<"+          Version: D1       +"<<endl
		<<"+                            +"<<endl
		<<"=============================="<<endl
		<<" ";
		pressToContinue();
		system("cls");
		
		cout<<"Enter Map number:"<<endl;
}






//Prints the map on the screen
void printMap(string map){
	for(int i; i<map.size(); ++i){

			if(map[i]=='W') cout<<(char)219;
			else if(map[i]=='$') cout<<(char)36; 
			else if(map[i]=='B') cout<<(char)61;
			else if(map[i]=='#') cout<<(char)89;
			else if(map[i]=='.') cout<<" ";
			else if(map[i]=='\n') cout<<"\n";
			else if(map[i]=='V') cout<<" ";
	}
}





/*****MAIN PROGRAM******/

int main(void){
	string mapNumber="1";
	showTitle();
	cin>>mapNumber;
	system("cls");
	
	
	string map="";
	string fileName="map";
	fileName +=mapNumber;
	fileName +=".txt";
	
	//Check file exists
	if(ifstream(fileName.c_str())){
	
		//open File
		ifstream inmap(fileName.c_str());
	
		//Read each lines
		string line;
		while(getline(inmap,line)){
			map += line;
			map +="\n";
		}
	}
	else{
		cout<<"The map: "<<fileName<<" does not exist";
		exit(1);
	}
		
	printMap(map);
	cout<<endl<<endl;

//
//
//	for(int i=0; i<300; ++i){
//		cout<<i<<": "<<(char)i<<endl;
//	}	

}





//Function
void pressToContinue(void){
    cout<<"Please ENTER to continue...";
    char c;
	cl();
	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
	while ((c = getchar()) != '\n' && c != EOF) { }
	cl();
}
